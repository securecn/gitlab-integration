# Gitlab Integration with SecureCN

This is set of templates showcasing how to use [SecureCN](https://securecn.cisco.com/) in GitLab CI/CD.

The documentation can be found [here](https://securecn.readme.io/docs/gitlab-pipeline-example).

## Runner config example

```toml
concurrent = 1
check_interval = 0
[session_server]
  session_timeout = 1800
[[runners]]
  name = "gitab-runner-instance-1"
  url = "https://gitlab.com/"
  token = "YOUR_TOKEN"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "alpine/helm"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/certs/client", "/cache"]
    shm_size = 0
```
